This is an ansible role to deploy snmpd on Debian-based systems (including Ubuntu) in a manner which is suitable for deployment in IPv6-first networks.

A typical configuration of a host would look something like this:

    snmp_listen: "udp6:[2001:db8:1000:2000::1]"
    snmp_syscontact: fixme@example.com
    snmp_syslocation: FIXME
    snmp_users:
      - name: librenms
        pass: !vault |
            $ANSIBLE_VAULT;1.1;AES256
            YOUR-ANSIBLE-VAULT-TEXT-GOES-HERE
